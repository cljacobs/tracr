#' Functions to interrogate the call stack.
#'
#' @description
#' These functions provide limited access to a function's environment. They are
#' convenience functions. For more direct access see the help pages linked at
#' the end of this page.
#'
#' @name caller
#' @seealso [base::sys.parent()]
NULL

#' @details
#' `caller` gets the name of the calling function. This function is very similar
#' to the [base::sys.call()] function, except that it returns the function's
#' name rather than the call itself. If the call is `NULL`, as when called from
#' an interative R shell the string `"script"` is returned.
#'
#' As with the [base::sys.call()] function, this function will never return
#' itself. That is, `caller(0)` will return the name of the calling function.
#'
#' @rdname caller
#' @export caller
#' @usage
#'   caller(n = 0)
#' @param n
#'   The frame number on the call stack. If negative, the relative frame number
#'   backward from the current frame.
caller <- function (n = 0) {
  # get the call (but looking back one)
  nframes <- sys.nframe()
  # skip/remove this function call when looking "forward"/"backward"
  n <- if (nframes == n) n + 1 else if (n <= 0) n - 1 else n
  call <- sys.call(which = n)
  # extract the call name as a string
  caller <- if (is.null(call)) "script" else as.character(call[[1]])
  return(caller)
}

#' @details
#' `script` gets the name of the executing script. If called from an interactive
#' REPL, this function returns `NA_character_`. Otherwise the name is parsed
#' from the command line arguments and returned as a string. The name, including
#' the file path, is returned as-is, without (e.g.) canonicalization.
#'
#' @rdname caller
#' @export script
#' @usage script()
script <- function () {
  # comb through the options and filter out the `--file` file
  options <- commandArgs(trailingOnly = FALSE)
  file.pattern <- '--file='
  name <- sub(file.pattern,'',options[grepl(file.pattern,options)])
  name <- if (length(name) == 0) NA_character_ else name
}
