<!-- DO NOT EDIT! README.md is auto-generated from README.rmd via: `rmarkdown::render("README.rmd")`. -->

# tracr

Unsophisticated call stack tracing.

## Overview

tracr is an extremely simple package designed to help R script hackers
interrogate the call stack. Currently, there are only two convenience
functions this package provides (see usage below).

## Installation

tracr is available via GitLab @
[cljacobs/tracr](https://gitlab.com/cljacobs/tracr).

``` r
# Installing with devtools:
library(devtools)
devtools::install_gitlab("cljacobs/tracr")
```

## Usage

Getting the name of script that was invoked (e.g., via `Rscript
<file.r>`) is easily done with `script`:

``` r
script() # NA from command line
```

Getting the name of the calling function can be done with `caller`. Like
`base::sys.call`, this function will accept as an argument a single
integer, which indicates the number on the calling stack to find the
caller of.

``` r
caller()
mycaller <- function () { caller(n = -1); } # 0 = "mycaller"
mycaller()
myfun <- function () { return(mycaller()); }
myfun()
```

<!-- This file's format is shamelessly copied from the [tidyverse](https://www.tidyverse.org/) collection of packages. -->
